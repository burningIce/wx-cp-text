package com.tianzong.zm.weixincp;

import cn.hutool.core.io.file.FileAppender;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpUser;
import me.chanjar.weixin.cp.bean.external.WxCpUserExternalGroupChatInfo;
import me.chanjar.weixin.cp.bean.external.WxCpUserExternalGroupChatList;
import me.chanjar.weixin.cp.bean.external.contact.WxCpExternalContactInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.List;

@SpringBootTest
class WeiXinCpApplicationTests {

    @Autowired
    WxCpService wxCpService;

    @Test
    void groupChatApiTest() {
        try {
            WxCpUserExternalGroupChatList groupChatList = wxCpService.getExternalContactService()
                    .listGroupChat(0, 10, 0, null, null);
            File file = new File("/Users/lujingpo/IdeaProjects/weixin-cp/groupChatList.txt");
            FileAppender appender = new FileAppender(file, 16, true);
            for (WxCpUserExternalGroupChatList.ChatStatus chatStatus : groupChatList.getGroupChatList()) {
                appender.append(chatStatus.getChatId()+","+ chatStatus.getStatus());
            }
            appender.flush();
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

    }

    @Test
    void groupInfoTest() throws WxErrorException {
        WxCpUserExternalGroupChatInfo groupChatInfo = wxCpService.getExternalContactService().getGroupChat("wrgdM9CQAA5IAWwq31PTvBKk2ZSICfww");
        WxCpUserExternalGroupChatInfo.GroupChat groupChat = groupChatInfo.getGroupChat();
        System.out.println(groupChat);
    }

    @Test
    void msgAuditGroupChat() throws WxErrorException {
        //MoXiang
        WxCpUser moXiang = wxCpService.getUserService().getById("MoXiang");
        System.out.println(moXiang);
    }

    @Test
    void testUserInfo() throws WxErrorException {
        List<String> moXiang = wxCpService.getExternalContactService().listExternalContacts("MoXiang");
        String userId = moXiang.get(0);
        WxCpExternalContactInfo contactDetail = wxCpService.getExternalContactService().getContactDetail(userId);
        System.out.println(contactDetail);
    }
}
