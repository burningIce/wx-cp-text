package com.tianzong.zm.weixincp.config;

import me.chanjar.weixin.common.redis.RedisTemplateWxRedisOps;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.api.impl.WxCpServiceImpl;
import me.chanjar.weixin.cp.config.impl.WxCpRedissonConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author lujingpo
 */
@Configuration
public class WxCpConfiguration {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Bean
    public WxCpService wxCpService() {
        RedisTemplateWxRedisOps redisOps = new RedisTemplateWxRedisOps(stringRedisTemplate);

        WxCpRedissonConfigImpl config = new WxCpRedissonConfigImpl(redisOps, "");
        config.setCorpId("ww4e997e4ed6647bb4");
        config.setAgentId(0);
        config.setCorpSecret("lTzjugwbI2FtCtI_OcBniMrKA7VUPAOcBm3wcZhnY48");
        WxCpServiceImpl wxCpService = new WxCpServiceImpl();
        wxCpService.setWxCpConfigStorage(config);
        return wxCpService;
    }

}
