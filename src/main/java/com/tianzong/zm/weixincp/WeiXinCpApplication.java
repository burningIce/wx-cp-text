package com.tianzong.zm.weixincp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeiXinCpApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiXinCpApplication.class, args);
    }

}
